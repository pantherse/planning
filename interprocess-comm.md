# Inter-process Communication

This document defines the inter-process communication protocol for Hathi modules

## Message format

Suggest using Extended Backus-Naur form to define syntax. For example:

Message := MSG Type , Payload Size , Payload
MSG Type  = ? 8-bits message identifier as defined by each message ?
Payload Size = ? 64-bits size for the Payload in bytes ?
Payload = ? Messsage content as defined by each message ?

## Messages

Below are the list of messages. For the purpose of this documentation, messages between components implies that it is routed through the router. For messages intended for the instance router itself, the router will be specified as one of the endpoint.

* Authenticate user
* Retrieve collection

### Authenticate user

This message is used to authenticate a user.

MSG Type: 1

![authentication diagram](interprocess-comm-diagrams/msg1.png)

#### Aunt Tillie

This is to authenticate user where the instance stores both the public and private key for the user.

    Payload = authenticate | response 
    authenticate = Username , Password (* Message from client to identity manager *)
    Username = ? Null-terminated UTF-8 string ? (* User's username *)
    Password = ? Null-terminated UTF-8 string ? (* User's private key password *)
    response = State , SUCCESS_MSG | FAILURE_MSG (* Message from identity manager to client *)
    State = ? 8-bits 1 or 0 ?
    SUCCESS_MSG = session key (* Only when State = 1 *)
    session key = TBD
    FAILURE_MSG = ? Failure codes TBD ?