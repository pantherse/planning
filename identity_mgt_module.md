# Identity Management Module
The purpose of this module is to handler user authentication and access control.

## Data structures

### User identity and authentication

* username: string, unique
* public key: blob
* private key: blob (optional)
* algorithm: byte
* display name: string, non-unique

### Available ACL

* ACL_ID: Unique identifier for record.
* service ID: blob to identify a service.
* ACL: byte
* Display text: string

### User ACL

* username: References "username" in "User identity and authentication"
* ACL_ID: References "ACL_ID" in "Available ACL"

## Configuration file

Use a json-file formatted text file.

### Configuration items

* router = Hathi instance router connection info
    * host = router's hostname
    * port = router's listening port
* db = Database server connection object
    * host = Database's hostname
    * port = Optional: DB port. If not specified, use the DB server's default port
    * username = DB login name
    * password = DB login password
    * name = Name of the identity management database

## Public key

Use RSA 2048 key size

## Key generation

Generate a 2048 key. On success, store the generated key pair in memory. On failure, log the cause.

## Private key storage

Generate a PKCS8 private key. Password-encrypt the private key using AES-256 encryption. On success, a PKCS8 null-terminated string is returned. On failure, throw runtime error containing the cause in the exception message.