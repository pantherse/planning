# Hathi architecture

## Modules:

* Object server: The actual server that handles all of the ActivityPub interactions

* Identity Manager: Responsible for authentication, session management, actor creation, etc.

* Router: The world facing module that clients are meant to talk to.

* Client

## Object server:

Store and process ActivityPub objects

### Primary tasks:

* POST object

* GET object

## Identity Manager:

User management

### Primary tasks:

* create Identity

* delete Identity

* create Actor

* delete Actor

* grant permission to Identity for Actor

* revoke permission to Identity for Actor

* is session token valid for Operation?

## Router:

Module that will route messages between client and the different server-side components.

### Primary tasks:

* Listen for connection from clients

* Connect and track object server, and Identity Manager modules for Hathi instance.

* create Identity

* delete Identity

* create Actor

* delete Actor

* grant permission to Identity for Actor

* revoke permission to Identity for Actor

* POST object

* GET object

## Client

User interface
