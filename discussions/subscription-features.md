Subscriptions
-------------

- Actors should be able to subscribe/unsubscribe to
  - feeds
  - people
  - groups
  - tags?

### Questions:
- What are 'feeds' exactly, from a hathi perspective?
  - A service Actor running as daemon, set up by some user crawl the net or the hathi sphere for content, and then send out automated messages?
  - A service Actor that (any) users can send things to and it will spread them?
  - A newspaper outlet, kind of peer-to-peer journalism?
  - All of the above?

- Should users be able to subscribe to tags?
  - In that case, how?
  - Would it not be more straightforward for users to be able to query for certain tags at will?
  - ..And have those searches automatically updated with new results perhaps?
