Searches
----------

What, and how, to query.

We are looking to be able to search for  

- feeds
- people
- groups
- (perhaps) tags

Feeds, people and groups are all Actors.
So we should be able to do this with a search by name - perhaps also text-search through profile? And/or, and possibly the most practical route, be able to have tags on Actors, and be able to search on Actors using those tags as search terms.
Should Actors other than the Actors themselves be able to set tags`on themselves?
Possibly a moot point; if tagging services exists, anyone can potentially tag anything, anyway.
But owner-defined tags might be more well-defined to search by.
