Task list for the Great Refactor
---------------------------------

-  [x] Object Validator (COMPLETE)  
   Reason: the current system doesn't truly validate; instead it abuses Go's type system to approximate a validator. Also the current system implements a subset which was chosen to limit complexity in the early stages.  

-  [x] DB format rewrite + Struct format rewrite (In Progress)  
   Blocked on: Object Validator  
   Reason: the current system cannot interoperate with other systems, or even a later version of Hathi. All fields are hardcoded.  

-  [ ] Sub-object detector (sort of complete)  
   Reason: There are several ways that one object can embed another. This will detect which one is being used so they can be properly handled. This is one of the areas where a subset was chosen and is now being expanded.  

-  [x] Recursive storage (COMPLETE)  
   Blocked on: DB format rewrite  
   Reason: We have to store sub-objects somehow ;-)  

-  [x] ID-counter storage (COMPETE)  
   Blocked on: DB format rewrite  
   Reason: The server needs to not start its new-ID-generator at zero every time it is run.  

-  [ ] Propagation storage  
   Blocked on: DB format rewrite  
   Reason: The server needs to keep track of what it needs to propagate to remote servers despite shut down.  

------------------------------------------------------------------------------

Object Validator (COMPLETE)  
Reason: the current system doesn't truly validate; instead it abuses Go's type system to approximate a validator. Also the current system implements a subset which was chosen to limit complexity in the early stages.  

DB format rewrite + Struct format rewrite (In Progress)  
Blocked on: Object Validator  
Reason: the current system cannot interoperate with other systems, or even a later version of Hathi. All fields are hardcoded.  

Sub-object detector (sort of complete)  
Reason: There are several ways that one object can embed another. This will detect which one is being used so they can be properly handled. This is one of the areas where a subset was chosen and is now being expanded.  

Recursive storage (COMPLETE)  
Blocked on: DB format rewrite  
Reason: We have to store sub-objects somehow ;-)  

ID-counter storage (COMPETE)  
Blocked on: DB format rewrite  
Reason: The server needs to not start its new-ID-generator at zero every time it is run.  

Propagation storage  
Blocked on: DB format rewrite  
Reason: The server needs to keep track of what it needs to propagate to remote servers despite shut down.  